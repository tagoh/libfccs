/* 
 * fccs-compare.c
 * Copyright (C) 2017 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <tagoh@redhat.com>
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN
 * IF THE COPYRIGHT HOLDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE COPYRIGHT HOLDER HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
#include <stdio.h>
#include <stdlib.h>
#include "fccs.h"

int
main(int argc, char **argv)
{
	const FcCharSet *cs;
	FcFontSet *fs;
	int i;
	FcChar32 oc;

	if (argc < 3) {
		printf("Usage: %s <charset> <file>\n", argv[0]);
		return 1;
	}
	cs = fccs_get_charset(argv[1]);
	oc = FcCharSetCount(cs);
	printf("%s: %d chars\n", argv[1], oc);
	fs = FcFontSetCreate();
	FcFileScan(fs, NULL, NULL, NULL, (const FcChar8 *)argv[2], FcTrue);
	for (i = 0; i < fs->nfont; i++) {
		FcPattern *pat = fs->fonts[i];
		FcChar8 *file;
		FcCharSet *fcs;
		FcChar32 c;

		if (FcPatternGetString(pat, FC_FILE, 0, &file) != FcResultMatch) {
			fprintf(stderr, "E: Unable to obtain the necessary information from font\n");
			goto bail;
		}
		if (FcPatternGetCharSet(pat, FC_CHARSET, 0, &fcs) != FcResultMatch) {
			fprintf(stderr, "E: Unable to obtain the necessary information from font\n");
			goto bail;
		}
		c = FcCharSetSubtractCount(cs, fcs);
		printf("%s: coverage %.2f%% (%d missing)\n", file, (oc - c) * 100.0 / oc, c);
	}
  bail:
	FcFontSetDestroy(fs);

	return 0;
}
