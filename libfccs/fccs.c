/* 
 * fccs.c
 * Copyright (C) 2017 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <tagoh@redhat.com>
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN
 * IF THE COPYRIGHT HOLDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE COPYRIGHT HOLDER HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include "fccs-private.h"

static FccsCache *
fccs_cache_open(const char *file)
{
	int fd;
	FccsCache *cache = NULL;
	struct stat fd_stat;
	FcBool allocated = FcFalse;

	if ((fd = open(file, O_RDONLY)) < 0)
		return NULL;
	if (fstat(fd, &fd_stat) < 0)
		goto bail;
	cache = mmap(NULL, fd_stat.st_size, PROT_READ, MAP_SHARED,  fd, 0);
	if (cache == MAP_FAILED)
		cache = NULL;
	if (!cache)
	{
		cache = malloc(fd_stat.st_size);
		if (!cache)
			goto bail;
		if (read(fd, cache, fd_stat.st_size) != fd_stat.st_size) {
			free(cache);
			cache = NULL;
			goto bail;
		}
		allocated = FcTrue;
	}
	if (cache->magic != FCCS_MAGIC_MMAP ||
	    cache->version < FCCS_VERSION ||
	    cache->size != (intptr_t)fd_stat.st_size) {
		if (allocated)
			free(cache);
		else
			munmap(cache, fd_stat.st_size);
		cache = NULL;
		goto bail;
	}
	if (allocated)
		cache->magic = FCCS_MAGIC_ALLOC;
  bail:
	close(fd);
	return cache;
}

static FccsCache *
fccs_cache_load(const char *charset)
{
	static const char *datadir = FCCSDATADIR;
	static const char *dbgdatadir = FCCSDBGDATADIR;
	char *f;
	size_t dlen = strlen(datadir);
	size_t len = strlen(charset);
	FccsCache *cache;

	f = malloc(dlen + 1 + len + 5 + 1);
	if (!f)
		return NULL;
	strncpy(f, datadir, dlen);
	f[dlen] = '/';
	strncpy(&f[dlen+1], charset, len);
	strncpy(&f[dlen+1+len], ".fccs", 5);
	f[dlen+1+len+5] = 0;
	if (!(cache = fccs_cache_open(f))) {
		free(f);
		dlen = strlen(dbgdatadir);
		f = malloc(dlen + 1 + len + 5 + 1);
		if (!f)
			return NULL;
		strncpy(f, dbgdatadir, dlen);
		f[dlen] = '/';
		strncpy(&f[dlen+1], charset, len);
		strncpy(&f[dlen+1+len], ".fccs", 5);
		f[dlen+1+len+5] = 0;
		if (!(cache = fccs_cache_open(f))) {
			free(f);
			return NULL;
		}
	}
	return cache;
}

const FcCharSet *
fccs_get_charset(const char *charset)
{
	const FccsCache *cache = fccs_cache_load(charset);
	const FcCharSet *cs;

	if (!cache)
		return NULL;
	/* XXX: cache needs to be munmap'd */
	cs = FcEncodedOffsetToPtr(cache, cache->cs, FcCharSet);

	return cs;
}
