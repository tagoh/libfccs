/* 
 * fccs-private.h
 * Copyright (C) 2017 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <tagoh@redhat.com>
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN
 * IF THE COPYRIGHT HOLDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE COPYRIGHT HOLDER HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
#ifndef __FCCS_PRIVATE_H__
#define __FCCS_PRIVATE_H__

#include "fccs.h"
#include "fcstdint.h"

#ifndef FcMatchKindEnd
#define FcMatchKindEnd	FcMatchScan + 1
#endif

#include "fcint.h"

#define FCCS_MAGIC_MMAP		0xFCC5FCC5
#define FCCS_MAGIC_ALLOC	0x5CCF5CCF
#define FCCS_VERSION		1

struct _FccsCache {
	unsigned int magic;
	int          version;
	intptr_t     size;
	intptr_t     cs;
};

#endif /* __FCCS_PRIVATE_H__ */
