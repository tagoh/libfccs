#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fontconfig/fontconfig.h>
#include "fccs-private.h"

int FcDebugVal;

static FcCharSet *
parse(char *file, int column)
{
	FILE *fp;
	char buf[1024], *s, *ss, *e;
	FcCharSet *ret = NULL;
	int c, line = 0, i;

	if ((fp = fopen(file, "rb")) == NULL)
		return NULL;
	ret = FcCharSetCreate();
	while (1) {
		line++;
		c = column;
		if (!fgets(buf, 1024, fp))
			break;
		s = strchr(buf, '#');
		if (s)
			*s = 0;
		s = buf;
		if (*s == '' || *s == '\r' || *s == '\n')
			*s = 0;
		if (!*s)
			c = -1;
		while (c > 0 && *s) {
			while (s[0] && !isspace(s[0]))
				s++;
			while (s[0] && isspace(s[0]))
				s++;
			c--;
		}
		if (c >= 0 && !*s) {
			fprintf(stderr, "E: No column expected: %s: line %d\n", file, line);
			goto bail;
		}
		if (!*s)
			continue;
		ss = s;
		while (ss[0] && !isspace(ss[0]))
			ss++;
		if (*ss)
			*ss = 0;
		errno = 0;
		i = strtod(s, &e);
		if (i == 0 && e && errno != 0) {
			fprintf(stderr, "E: Unable to parse a number: %s: line %d\n", file, line);
			goto bail;
		} else if (strncmp(&e[-1], "..", 2) == 0) {
			int end, j;

			errno = 0;
			ss = &e[2];
			end = strtod(ss, &e);
			if (end == 0 && e && errno != 0) {
				fprintf(stderr, "E: Unable to parse a number: %s: line %d\n", file, line);
				goto bail;
			}
			for (j = i; j <= end; j++) {
				FcCharSetAddChar(ret, i);
			}
			continue;
		} else if (*e == '\r' || *e == '\n' || *e == 0) {
		} else {
			fprintf(stderr, "E: Invalid syntax: %s: line %d\n", file, line);
			goto bail;
		}
		FcCharSetAddChar(ret, i);
	}
	fclose(fp);

	return ret;
  bail:
	FcCharSetDestroy(ret);
	fclose(fp);

	return NULL;
}

int
main(int argc, char **argv)
{
	FcCharSet *fcs, *fcs_serialized;
	FcSerialize *serialize;
	FccsCache *bin;
	FILE *fp;

	if (argc < 4)
		return 1;
	fcs = parse(argv[1], atoi(argv[2]));
	if (!fcs)
		return 1;
	if (argc == 6) {
		FcCharSet *sub, *rcs;

		sub = parse(argv[4], atoi(argv[5]));
		rcs = FcCharSetSubtract(fcs, sub);
		FcCharSetDestroy(sub);
		FcCharSetDestroy(fcs);
		fcs = rcs;
	}
	serialize = FcSerializeCreate();
	FcSerializeReserve(serialize, sizeof (FccsCache));
	FcCharSetSerializeAlloc(serialize, fcs);

	bin = malloc(serialize->size);
	memset(bin, 0, serialize->size);
	bin->magic = FCCS_MAGIC_MMAP;
	bin->version = FCCS_VERSION;
	bin->size = serialize->size;
	serialize->linear = bin;

	fcs_serialized = FcCharSetSerialize(serialize, fcs);
	bin->cs = FcPtrToOffset(bin, fcs_serialized);

	if ((fp = fopen(argv[3], "wb")) == NULL)
		return 1;
	fwrite(bin, serialize->size, 1, fp);
	fclose(fp);

	free(bin);
	FcSerializeDestroy(serialize);
	FcCharSetDestroy (fcs);

	return 0;
}
